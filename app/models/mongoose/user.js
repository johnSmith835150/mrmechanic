const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    name: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    mobile: { type: String, required: true },
    date_of_birth: { type: String, required: true },
    image: { type: String, required: true },
    email_verified: { type: Boolean, default: true },
    remember_token: { type: String },
    active: { type: Boolean, default: true },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'users' });

module.exports = mongoose.model('User', userSchema);
