const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const carDatabaseSchema = new Schema({
    brand: { type: String, required: true },
    model: { type: String, required: true },
    year: { type: Number, required: true },
}, { collection: 'cars_database' });

module.exports = mongoose.model('CarDatabase', carDatabaseSchema);
