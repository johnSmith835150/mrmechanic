const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contractSchema = new Schema({
    userId: { type: String, required: true },
    partner: { type: Object, required: true },
    price: { type: Number, required: true },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'contract' });

module.exports = mongoose.model('Contract', contractSchema);
