const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const carSchema = new Schema({
    userId: { type: String, required: true },
    manufacturer: { type: String, required: true },
    model: { type: String, required: true },
    year: { type: Number, required: true },
    createdAt: { type: String, required: true },
    updatedAt: { type: String, required: true }
}, { collection: 'cars' });

module.exports = mongoose.model('Car', carSchema);
