const mongoose = require('mongoose');


mongoose.connect('mongodb+srv://mrmechanicUser:eWqGrEN7Ah3Mt9rq@cluster0-c2jim.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true });
const db = mongoose.connection;

db.once('open', function(){
    console.log(">>>>>>>>>>>>>>> conncted to mongoDB <<<<<<<<<<<<<<<");
});

db.on('errors', function(err){
    console.log(err);
});

var models = {};
models.User = require('./user');
models.Car = require('./car');
models.CarDatabase = require('./carDatabase');
models.Contract = require('./contract');

module.exports = models;
