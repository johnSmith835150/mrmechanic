const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const utils = require('../../utils/util');
const userkey = require('../../config/keys').api.userSecret;

var exports = module.exports = {};
exports.User = null;


exports.validate = (req, email=false, password=false, signup=false) => {
    var errors = [];

    if (email)
        req.checkBody('email', 'Email is not valid').notEmpty().isEmail();

    if (password)
        req.checkBody('password', 'Invalid password must be at least 4 characters').notEmpty().isLength({min: 4});

    if (signup) {
        req.checkBody('name', 'Invalid name must be at least 4 characters').notEmpty().isLength({min: 4});
        req.checkBody('mobile', 'Mobile number is not valid').notEmpty().isMobilePhone();

        if (req.body.password !== req.body.passwordConfirmation)
            errors.push('Invalid password does not match password confirmation');

        if (!req.body.dateOfBirth)
            errors.push('Iinvalid date of birth age must be at least 18');
        else {
            if (!utils.isValidDate(req.body.dateOfBirth))
                errors.push('Invalid date of birth age must be at least 18');
        }
    }

    var err = req.validationErrors();

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        return errors;
    }

    return false;
};


exports.request = async (req, res) => {
    var path = req.path;
    var result = 404;

    if (path === "/login") {
        let errors = exports.validate(req, email=true, password=true);
        
        if (errors)
            result = { status: false, message: "Login was unsuccessful", token: null, errors: errors };
        else
            result = await exports.login(req.body.email, req.body.password);
    }
    else if (path === "/signup") {
        let errors = exports.validate(req, email=true, password=true, signup=true);

        if (errors)
            result = { status: false, message: "Signup was unsuccessful", token: null, errors: errors };
        else
            result = await exports.signup(req.body.name, req.body.email, req.body.password, req.body.mobile, req.body.dateOfBirth);
    }
    
    if (result === 404)
        res.sendStatus(404);
    else if (result === 403)
        res.sendStatus(403);
    else
        res.json(result);
};


exports.login = async (email, password) => {
    var user = await exports.User.find({ email: email.toLowerCase() });
    if (user.length === 0)
        return {status: false, message: "Login was unsuccessful", token: null, errors: [
            { message: "Email doesn't exist", code: 120 }
        ]};

    user = user[0];

    if (utils.isValidPassword(user.password, password)) {
        let userData = {
            id: user.id,
            email: user.email
        };
    
        var token = await jwt.sign({userData}, userkey, { expiresIn: '36500d' });
        if (token) {
            user.remember_token = token;
            var savedUser = await user.save();
            if (savedUser)
                return {status: true, message: "Login was successful", userId: savedUser.id, token: token, errors: null };
            else
                return {status: false, message: "Login was unsuccessful", token: null, errors: [
                    { message: "Failed to update token in user data, please login again", code: 100 }
                ]};
        }
        else
            return {status: false, message: "Login was unsuccessful", token: null, errors: [
                { message: "Couldn't create token, please try again", code: 100 }
            ]};
    }
    else
        return {status: false, message: "Login was unsuccessful", token: null, errors: [
            { message: "Password doesn't match", code: 130 }
        ]};
};


exports.signup = async (name, email, password, mobile, dateOfBirth) => {
    let imagePath = path.join(process.cwd(), 'assests/defaultUser.png');
    let image = fs.readFileSync(imagePath).toString('base64');
    var user = new exports.User({
        name: name,
        email: email.toLowerCase(),
        password: utils.generateHash(password),
        mobile: mobile,
        date_of_birth: dateOfBirth,
        image: image,
        createdAt: utils.now(),
        updatedAt: utils.now()
    });

    var savedUser = await user.save();
    if (savedUser){
        let userData = {
            id: savedUser.id,
            email: savedUser.email
        };

        var token = await jwt.sign({userData}, userkey, { expiresIn: '36500d' });
        if (token) {
            savedUser.remember_token = token;
            var updatedUser = await savedUser.save();
            if (updatedUser)
                return {status: true, message: "Signup was successful", userId: updatedUser.id, token: token, errors: null };
            else
                return {status: false, message: "Signup was successful", token: null, errors: [
                    { message: "Failed to update token in user data, please login again", code: 100 }
                ]};
        }
        else
            return {status: false, message: "Account was created but failed to login, please login again", token: null, errors: [
                { message: "Couldn't create token, please try again", code: 100 }
            ]};
    }
    else
        return {status: false, message: "Signup was unsuccessful", token: null, errors: [
            { message: "Failed to create account please try again", code: 110 }
        ]};
};
