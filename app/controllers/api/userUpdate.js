const jwt = require('jsonwebtoken');
const utils = require('../../utils/util');
const userkey = require('../../config/keys').api.userSecret;

var exports = module.exports = {};
exports.User = null;


exports.validate = (req, updateName=false, updateMobile=false, updateDateOfBirth=false) => {
    var errors = [];

    req.checkBody('token', 'Auth token must be provided').notEmpty();

    if (updateName)
        req.checkBody('name', 'Invalid name must be at least 4 characters').notEmpty().isLength({min: 4});

    if (updateMobile)
        req.checkBody('mobile', 'Mobile number is not valid').notEmpty().isMobilePhone();
    
    if (updateDateOfBirth) {
        if (!req.body.dateOfBirth)
            errors.push('Invalid date of birth age must be at least 18');
        else {
            if (!utils.isValidDate(req.body.dateOfBirth))
                errors.push('Invalid date of birth age must be at least 18');
        }
    }

    var err = req.validationErrors();

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        return errors;
    }

    return false;
};


exports.request = async (req, res) => {
    var path = req.path;
    var result = 404;

    if (path === "/updateName") {
        errors = exports.validate(req, updateName=true);
        if (errors)
            result = { status: false, message: "Name update was unsuccessful", errors: errors };
        else
            result = await exports.updateName(req.body.name, req.body.token);
    }
    else if (path === "/updateMobile") {
        errors = exports.validate(req, updateName=false, updateMobile=true);
        
        if (errors)
            result = { status: false, message: "Mobile update was unsuccessful", errors: errors };
        else
            result = await exports.updateMobile(req.body.mobile, req.body.token);
    }
    else if (path === "/updateDateOfBirth") {
        errors = exports.validate(req, updateName=false, updateMobile=false, updateDateOfBirth=true);
        if (errors)
            result = { status: false, message: "DateOfBirth update was unsuccessful", errors: errors };
        else
            result = await exports.updateDateOfBirth(req.body.dateOfBirth, req.body.token);
    }
    
    if (result === 404)
        res.sendStatus(404);
    else if (result === 403)
        res.sendStatus(403);
    else
        res.json(result);
};


exports.updateName = async (name, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            user.name = name;
            user.updatedAt = utils.now();

            var savedUser = await user.save();
            if (savedUser)
                return { status: true, message: "User data was updated successfully", errors: null };
            else
                return {status: false, message: "Failed to updated user data please try again", errors: [
                    { message: "Falied to save user changes, please try again", code: 140 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
};


exports.updateMobile = async (mobile, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            user.mobile = mobile;
            user.updatedAt = utils.now();

            var savedUser = await user.save();
            if (savedUser)
                return { status: true, message: "User data was updated successfully", errors: null };
            else
                return {status: false, message: "Failed to updated user data please try again", errors: [
                    { message: "Falied to save user changes, please try again", code: 140 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
};


exports.updateDateOfBirth = async (dateOfBirth, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            user.date_of_birth = dateOfBirth;
            user.updatedAt = utils.now();

            var savedUser = await user.save();
            if (savedUser)
                return { status: true, message: "User data was updated successfully", errors: null };
            else
                return {status: false, message: "Failed to updated user data please try again", errors: [
                    { message: "Falied to save user changes, please try again", code: 140 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
};
