const jwt = require('jsonwebtoken');
const utils = require('../../utils/util');
const userkey = require('../../config/keys').api.userSecret;

var exports = module.exports = {};
exports.User = null;
exports.Contract = null;

exports.validate = (req, add=false, remove=false) => {
    var errors = [];

    req.checkBody('token', 'Auth token must be provided').notEmpty();

    if (add) {
        req.checkBody('userId', 'UserId must be provided').notEmpty();
        req.checkBody('partner', 'Manufacturer must be provided').notEmpty();
        req.checkBody('price', 'Price must be a number bigger than zero').notEmpty().isNumeric();

        if (this.body.price <= 0)
            errors.push("Price must be a number bigger than zero");
        
        
    }

    if (remove)
        req.checkBody('id', 'Car id must be provided').notEmpty();

    var err = req.validationErrors();

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        return errors;
    }

    return false;
};

exports.request = async (req, res) => {
    var path = req.path;
    var result = 404;

    if (path === "/getCars") {
        let errors = exports.validate(req, get=true);

        if (errors)
            result = { status: false, message: "User cars retrival was unsuccessful", errors: errors };
        else
            result = await exports.getCars(req.body.token);
    }
    else if (path === "/addCar") {
        let errors = exports.validate(req, get=false, add=true);

        if (errors)
            result = { status: false, message: "Car creation was unsuccessful", errors: errors };
        else
            result = await exports.updateProfileImage(req.file, req.body.token);
    }
    else if (path === "/deleteCar") {
        let errors = exports.validate(req, get=false, add=false, remove=true);

        if (errors)
            result = { status: false, message: "Car deletion was unsuccessful", errors: errors };
        else
            result = await exports.deleteCar(req.body.id, req.body.token);
    }
    
    if (result === 404)
        res.sendStatus(404);
    else if (result === 403)
        res.sendStatus(403);
    else
        res.json(result);
};

exports.getCars = async (token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            let cars = await exports.Car.find({ userId: String(user._id) });

            if (cars)
                return { status: true, message: "User cars was retrived sucessfully", cars: cars, errors: null };
            else
                return { status: true, message: "User cars was retrived sucessfully", cars: [], errors: null };
        }
        else
            return 403;
    }
    else
        return 403;
};


exports.addCar = async (userId, manufacturer, model, year, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user && String(user._id) == String(userId)) {
            let car = new exports.Car({
                userId: userId,
                manufacturer: manufacturer,
                model: model,
                year: year,
                createdAt: utils.now(),
                updatedAt: utils.now()
            });

            var savedCar = await car.save();
            if (savedCar)
                return { status: true, message: "Car was added succesfully", errors: null };
            else
                return { status: false, message: "Car was not added", errors: [
                    { message: "Failed to add car, please try again", code: 110 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
};

exports.deleteCar = async (id, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            await exports.Car.deleteOne({ _id: id });
            return { status: true, message: "User car was deleted sucessfully", errors: null };
        }
        else
            return 403;
    }
    else
        return 403;
};
