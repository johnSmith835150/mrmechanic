const fs = require('fs');
const path = require('path');
const jwt = require('jsonwebtoken');
const utils = require('../../utils/util');
const userKey = require('../../config/keys').api.userSecret;
const adminKey = require('../../config/keys').api.adminSecret;

var exports = module.exports = {};
exports.User = null;
exports.Car = null;
exports.CarDatabase = null;


exports.validate = (req, add=false, remove=false) => {
    var errors = [];

    req.checkBody('token', 'Auth token must be provided').notEmpty();

    if (add) {
        req.checkBody('manufacturer', 'Manufacturer must be provided').notEmpty();
        req.checkBody('model', 'Model must be provided').notEmpty();
        req.checkBody('year', 'Year must be between 1980 and now').notEmpty().isNumeric();

        let now = new Date();
        if (!(req.body.year >= 1980 && req.body.year <= now.getFullYear()))
            errors.push('Year must be between 1980 and now');
    }

    if (remove)
        req.checkBody('id', 'Car id must be provided').notEmpty();

    var err = req.validationErrors();

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        return errors;
    }

    return false;
};

exports.request = async (req, res) => {
    var path = req.path;
    var result = 404;

    if (path === "/createCarDatabase") {
        let errors = exports.validate(req);
        if (errors)
            result = 403;
        else
            result = await exports.createCarDatabase(req.body.token);
    }
    if (path === "/hasCars") {
        let errors = exports.validate(req);
        if (errors)
            result = { status: false, message: "Cars check was unsuccessful", errors: errors };
        else
            result = await exports.hasCars(req.body.token);
    }
    if (path === "/getCars") {
        let errors = exports.validate(req);
        if (errors)
            result = { status: false, message: "User cars retrival was unsuccessful", errors: errors };
        else
            result = await exports.getCars(req.body.token);
    }
    else if (path === "/addCar") {
        let errors = exports.validate(req, add=true);

        if (errors)
            result = { status: false, message: "Car creation was unsuccessful", errors: errors };
        else
            result = await exports.addCar(req.body.manufacturer, req.body.model, req.body.year, req.body.token);
    }
    else if (path === "/deleteCar") {
        let errors = exports.validate(req, add=false, remove=true);

        if (errors)
            result = { status: false, message: "Car deletion was unsuccessful", errors: errors };
        else
            result = await exports.deleteCar(req.body.id, req.body.token);
    }
    
    if (result === 404)
        res.sendStatus(404);
    else if (result === 403)
        res.sendStatus(403);
    else
        res.json(result);
};

exports.createCarDatabase = async (token) => {
    var authData = await jwt.verify(token, adminKey);
    if (authData) {
        let brands = await exports.CarDatabase.find().distinct('brand');

        database = {};
        for (var i = 0; i < brands.length; i++) {
            let brandModels = await exports.CarDatabase.find({ brand: brands[i] }).distinct('model');
            let models = {};
            for (var j = 0; j < brandModels.length; j++) {
                let years = await exports.CarDatabase.find({ brand: brands[i], model: brandModels[j] }).select('year -_id');
                for (var k = 0; k < years.length; k++)
                    years[k] = years[k].year;

                models[brandModels[j]] = years;
            }
            database[brands[i]] = models;
        }

        let databasePath = path.join(process.cwd(), 'car_database.json');
        fs.writeFileSync(databasePath, JSON.stringify(database));
        return {result: "database was generated successfully!!!"};
    }

    return 403;
};

exports.getCarDatabase = async (req, res) => {
    try {
        let databasePath = path.join(process.cwd(), 'car_database.json');
        let database = fs.readFileSync(databasePath);
        res.json(JSON.parse(database));
    } catch (error) {
        res.sendStatus(404);
    }
};

exports.hasCars = async (token) => {
    var authData = await jwt.verify(token, userKey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            let cars = await exports.Car.find({ userId: String(user._id) });

            if (cars.length > 0)
                return { status: true, message: "Cars check was sucessful", cars: true, errors: null };
            else
                return { status: true, message: "Cars check was sucessful", cars: false, errors: null };
        }
        else
            return 403;
    }
    else
        return 403;
};

exports.getCars = async (token) => {
    var authData = await jwt.verify(token, userKey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            let cars = await exports.Car.find({ userId: String(user._id) });

            if (cars)
                return { status: true, message: "User cars was retrived sucessfully", cars: cars, errors: null };
            else
                return { status: true, message: "User cars was retrived sucessfully", cars: [], errors: null };
        }
        else
            return 403;
    }
    else
        return 403;
};

exports.addCar = async (manufacturer, model, year, token) => {
    var authData = await jwt.verify(token, userKey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (!user)
            return 403
    
        let car = new exports.Car({
            userId: String(user._id),
            manufacturer: manufacturer,
            model: model,
            year: year,
            createdAt: utils.now(),
            updatedAt: utils.now()
        });

        var savedCar = await car.save();
        if (savedCar)
            return { status: true, message: "Car was added succesfully", id: savedCar._id, errors: null };
        else
            return { status: false, message: "Car was not added", errors: [
                { message: "Failed to add car, please try again", code: 110 }
            ]};
    }
    else
        return 403;
};

exports.deleteCar = async (id, token) => {
    var authData = await jwt.verify(token, userKey);
    if (authData) {
        let user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            await exports.Car.deleteOne({ _id: id });
            return { status: true, message: "User car was deleted sucessfully", errors: null };
        }
        else
            return 403;
    }
    else
        return 403;
};
