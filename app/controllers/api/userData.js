const jwt = require('jsonwebtoken');
const utils = require('../../utils/util');
const userkey = require('../../config/keys').api.userSecret;

var exports = module.exports = {};
exports.User = null;


exports.validate = (req, image=false, review=false) => {
    var errors = [];

    req.checkBody('token', 'Auth token must be provided').notEmpty();

    if (image) {
        if (!req.file)
            errors.push("No file provided");
    
        if (req.file && req.file.mimetype.slice(0, 5) !== "image")
            errors.push("Provided file must be of type image");
    }

    if (review) {
        req.checkBody('id', 'Id must be provided').notEmpty();
    }

    var err = req.validationErrors();

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];
        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        return errors;
    }

    return false;
};

exports.request = async (req, res) => {
    var path = req.path;
    var result = 404;

    if (path === "/details") {
        let errors = exports.validate(req);

        if (errors)
            result = { status: false, message: "User data retrival was unsuccessful", errors: errors };
        else
            result = await exports.details(req.body.token);
    }
    else if (path === "/getProfieImage")
    {
        let errors = exports.validate(req);

        if (errors)
            result = { status: false, message: "User data retrival was unsuccessful", errors: errors };
        else
            result = await exports.getProfileImage(req.body.token);
    }
    else if (path === "/reviewerDetails") {
        let errors = exports.validate(req, image=false, review=true);

        if (errors)
            result = { status: false, message: "User data retrival was unsuccessful", errors: errors };
        else
            result = await exports.getReviewerDetails(req.body.id, req.body.token);
    }
    else if (path === "/updateProfileImage") {
        let errors = exports.validate(req, image=true);

        if (errors)
            result = { status: false, message: "User data update was unsuccessful", errors: errors };
        else
            result = await exports.updateProfileImage(req.file, req.body.token);
    }
    
    if (result === 404)
        res.sendStatus(404);
    else if (result === 403)
        res.sendStatus(403);
    else
        res.json(result);
};


exports.details = async (token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token }).select('name email mobile date_of_birth image -_id');

        if (user)
            return { status: true, message: "User data was retrived sucessfully", user: user, errors: null };
        else
            return 403;
    }
    else
        return 403;
};

exports.getReviewerDetails = async (id, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token }).select('name -_id');

        if (user) {
            let otherUser = await exports.User.findOne({ _id: id }).select('name image -_id');
            if (otherUser)
                return { status: true, message: "User data was retrived sucessfully", user: otherUser, errors: null };
            else
                return { status: false, message: "User data was retrived unsucessfully", user: null, errors: [
                    { message: "User doesn't exist", code: 120 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
};

exports.getProfileImage = async (token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token }).select('image -_id');

        if (user)
            return { image: user.image };
        else
            return 403;
    }
    else
        return 403;
};

exports.updateProfileImage = async (image, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            user.image = image.buffer.toString('base64');
            user.updatedAt = utils.now();

            var savedUser = await user.save();
            if (savedUser)
                return { status: true, message: "User data was updated successfully", errors: null };
            else
                return {status: false, message: "User data update was unsuccessful", errors: [
                    { message: "Falied to save user changes, please try again", code: 140 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
}
