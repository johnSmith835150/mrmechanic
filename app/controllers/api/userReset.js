const jwt = require('jsonwebtoken');
const utils = require('../../utils/util');
const userkey = require('../../config/keys').api.userSecret;

var exports = module.exports = {};
exports.User = null;


exports.validate = (req, email=false, password=false, reset=false) => {
    var errors = [];

    req.checkBody('token', 'Auth token must be provided').notEmpty();

    if (email)
        req.checkBody('email', 'Email is not valid').notEmpty().isEmail();

    if (password)
        req.checkBody('password', 'Invalid password must be at least 8 characters').notEmpty().isLength({min: 8});

    if (reset) {
        req.checkBody('oldPassword', 'Invalid old password').notEmpty().isLength({min: 8});

        if (req.body.password !== req.body.passwordConfirmation)
            errors.push('Invalid password does not match password confirmation');
    }

    var err = req.validationErrors();

    if (err || errors.length) {
        if (err) {
            err.forEach(element => {
                errors.push(element.msg);
            });
            errors = [...new Set(errors)];

        }

        for (var i = 0; i < errors.length; i++)
            errors[i] = { message: errors[i], code: 50 };

        return errors;
    }

    return false;
};


exports.request = async (req, res) => {
    var path = req.path;
    var result = 404;

    if (path === "/updateEmail") {
        errors = exports.validate(req, email=true);
        if (errors)
            result = { status: false, message: "Email update was unsuccessful", errors: errors };
        else
            result = await exports.updateEmail(req.body.email, req.body.token);
    }
    else if (path === "/reset") {
        errors = exports.validate(req, email=false, password=true, reset=true);
        if (errors)
            result = { status: false, message: "Password reset was unsuccessful", token: null, errors: errors };
        else
            result = await exports.reset(req.body.oldPassword, req.body.password, req.body.token);
    }
    
    if (result === 404)
        res.sendStatus(404);
    else if (result === 403)
        res.sendStatus(403);
    else
        res.json(result);
};


exports.updateEmail = async (email, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if (user) {
            user.email = email.toLowerCase();
            user.updatedAt = utils.now();
            var savedUser = await user.save();

            if (savedUser) {
                let userData = {
                    id: savedUser.id,
                    email: savedUser.email
                };

                var token = await jwt.sign({userData}, userkey, { expiresIn: '36500d' });
                if (token) {
                    savedUser.remember_token = token;
                    var updatedUser = await savedUser.save();
                    if (updatedUser)
                        return { status: true, message: "Email update was successful", token: token, errors: null };
                    else
                        return {status: false, message: "Email update was successful", token: null, errors: [
                            { message: "Failed to login again, please login again using new email", code: 100 }
                        ]};
                }
                else
                    return {status: false, message: "Email update was successful", token: null, errors: [
                        { message: "Couldn't create token, please login again using new email", code: 100 }
                    ]};
            }
            else
                return {status: false, message: "Email update was unsuccessful", token: null, errors: [
                    { message: "Failed to update email, please try again", code: 140 }
                ]};
        }
        else
            return 403;
    }
    else
        return 403;
};


exports.reset = async (oldPassword, password, token) => {
    var authData = await jwt.verify(token, userkey);
    if (authData) {
        var user = await exports.User.findOne({ email: authData.userData.email, remember_token: token });

        if(user) {
            if (utils.isValidPassword(user.password, oldPassword)) {
                user.password = utils.generateHash(password);
                user.updatedAt = utils.now();
                var savedUser = await user.save();
    
                if (savedUser) {
                    let userData = {
                        id: savedUser.id,
                        email: savedUser.email
                    };
    
                    var token = await jwt.sign({userData}, userkey, { expiresIn: '36500d' });
                    if (token) {
                        savedUser.remember_token = token;
                        var updatedUser = await savedUser.save();
                        if (updatedUser)
                            return { status: true, message: "Password reset was successful", token: token, errors: null };
                        else
                            return {status: false, message: "Password reset was successful", token: null, errors: [
                                { message: "Failed to login again, please login again using new password", code: 100 }
                            ]};
                    }
                    else
                        return {status: false, message: "Password reset was successful", token: null, errors: [
                            { message: "Couldn't create token, please login again using new password", code: 100 }
                        ]};
                }
                else
                    return {status: false, message: "Password reset was unsuccessful", token: null, errors: [
                        { message: "Failed to reset password, please try again", code: 140 }
                    ]};
            }
            else
                return {status: false, message: "Password reset was unsuccessful", token: null, errors: [
                    { message: "Invalid old password doesn't match", code: 130 }
                ]};
        }
        else
            return 403;

    }
    else
        return 403;
};
