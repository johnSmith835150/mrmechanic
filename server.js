const morgan = require('morgan');
const express = require('express');
const env = require('dotenv').load();
const port = process.env.PORT || 8080;
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');

var app = require('express')();
var http = require('http').Server(app);

// express config
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());

app.use(morgan('combined'));

// Models
var models = require("./app/models/mongoose");

// setting models for API
require("./app/controllers/api/userAuth").User = models.User;
require("./app/controllers/api/userData").User = models.User;
require("./app/controllers/api/userUpdate").User = models.User;
require("./app/controllers/api/userReset").User = models.User;
require("./app/controllers/api/car").User = models.User;
require("./app/controllers/api/car").Car = models.Car;
require("./app/controllers/api/car").CarDatabase = models.CarDatabase;

const routes = require('./routes/web');
app.use('/', routes);

http.listen(port, function(){
    console.log('listening on *:' + port);
});
