const express = require('express');
const router = express.Router();
const multer = require("multer");
const userAuthApiController = require("../app/controllers/api/userAuth");
const userDataApiController = require("../app/controllers/api/userData");
const userUpdateApiController = require("../app/controllers/api/userUpdate");
const userResetApiController = require("../app/controllers/api/userReset");
const carApiController = require("../app/controllers/api/car");

const upload = multer();

// auth routes
router.post('/login', userAuthApiController.request);
router.post('/signup', userAuthApiController.request);
// data routes
router.post('/details', userDataApiController.request);
router.post('/reviewerDetails', userDataApiController.request);
router.post('/getProfieImage', userDataApiController.request);
router.post('/updateProfileImage', upload.single('file'), userDataApiController.request);
// update routes
router.post('/updateName', userUpdateApiController.request);
router.post('/updateMobile', userUpdateApiController.request);
router.post('/updateDateOfBirth', userUpdateApiController.request);
// reset routes
router.post('/updateEmail', userResetApiController.request);
router.post('/reset', userResetApiController.request);

router.get('/carDatabase', carApiController.getCarDatabase);
router.post('/createCarDatabase', carApiController.request);
router.post('/hasCars', carApiController.request);
router.post('/getCars', carApiController.request);
router.post('/addCar', carApiController.request);
router.post('/deleteCar', carApiController.request);


module.exports = router;
