const express = require('express');
const router = express.Router();

const api_routes = require('./api');
router.use('/api', api_routes);

module.exports = router;
